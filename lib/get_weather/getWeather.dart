import 'dart:math';

import 'package:dio/dio.dart';

class HttpGet {
  static String key = "f52e82684f47c3dd99b8b8c223d7c634";

  static Future getHttp(String nameCity) async {
    try {
      var response = await Dio().get(
          'http://api.openweathermap.org/data/2.5/weather?q=$nameCity&appid=$key');
      if (response.statusCode == 200) {
        return response.data;
      } else {
        print("Ошибка запроса");
        return "error";
      }
    } on DioError catch (e) {
      if (e.response.statusCode == 404) {
        return "errorInput";
      } else {
        return "emptyField";
      }
    }
  }
}
