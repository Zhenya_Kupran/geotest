import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_test/city/view.dart';
import 'package:my_test/di.dart' as di;

import 'city/logic.dart';

void main() {
  di.init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<MyBloc>(
      create: (context) => di.sl<MyBloc>(),
      child: MaterialApp(
        home: City(),
      ),
    );
  }
}
