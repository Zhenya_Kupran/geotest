import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_test/city/logic.dart';
import 'package:my_test/di.dart' as di;

class ViewWeather extends StatefulWidget {
  ViewWeather(this.info);
  var info = di.sl<Info>();
  @override
  _ViewWeatherState createState() => _ViewWeatherState();
}

class _ViewWeatherState extends State<ViewWeather> {
  var height;
  var width;

  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    MyBloc bloc = BlocProvider.of<MyBloc>(context);
    return Scaffold(
        body: Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("Название - ${widget.info.name}"),
            Text("Максимальная температура - ${widget.info.max}"),
            Text("Минимальная температура - ${widget.info.min}"),
            Text("Давление - ${widget.info.pressure}"),
            Text("Влажность - ${widget.info.humidity}"),
            Container(
              child: Center(
                child: ElevatedButton(
                  onPressed: () {
                    bloc.add("main");
                  },
                  child: Text("Вернуться назад"),
                ),
              ),
            )
          ],
        ),
      ),
    ));
  }
}
