import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_test/city/logic.dart';
import 'package:my_test/get_weather/getWeather.dart';
import 'package:my_test/info/viewInfo.dart';

class City extends StatefulWidget {
  @override
  _CityState createState() => _CityState();
}

class _CityState extends State<City> {
  TextEditingController inputCity = TextEditingController();
  FocusNode focusNode = FocusNode();

  var height;
  var width;
  @override
  Widget build(BuildContext context) {
    height = MediaQuery.of(context).size.height;
    width = MediaQuery.of(context).size.width;
    MyBloc bloc = BlocProvider.of<MyBloc>(context);
    return Scaffold(
      body: Center(child: BlocBuilder<MyBloc, Info>(
        builder: (BuildContext context, state) {
          if (state.status == 5) {
            return ViewWeather(state);
          }
          if (state.status == 4) {
            return Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            );
          }
          if (state.status == 1) {
            return Container(
              child: Center(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Не правильно ввели город"),
                  Container(
                    child: Center(
                      child: ElevatedButton(
                        onPressed: () {
                          bloc.add("main");
                        },
                        child: Text("Перейти к вводу города"),
                      ),
                    ),
                  )
                ],
              )),
            );
          } else if (state.status == 2) {
            return Container(
              child: Center(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Не ввели город"),
                  Container(
                    child: Center(
                      child: ElevatedButton(
                        onPressed: () {
                          bloc.add("main");
                        },
                        child: Text("Перейти к вводу города"),
                      ),
                    ),
                  )
                ],
              )),
            );
          } else {
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: width * 0.5,
                  child: TextFormField(
                    focusNode: focusNode,
                    controller: inputCity,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: "Введите город"),
                  ),
                ),
                SizedBox(
                  height: height * 0.01,
                ),
                Container(
                  child: Center(
                    child: ElevatedButton(
                      onPressed: () {
                        bloc.add(inputCity.text);
                      },
                      child: Text("Получить данные"),
                    ),
                  ),
                )
              ],
            );
          }
        },
      )),
    );
  }
}
