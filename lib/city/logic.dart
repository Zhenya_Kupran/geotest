import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_test/get_weather/getWeather.dart';
import 'package:my_test/di.dart' as di;

class MyBloc extends Bloc<String, Info> {
  MyBloc(initialState) : super(initialState);

  @override
  Stream<Info> mapEventToState(event) async* {
    if (event == "main") {
      var info = di.sl<Info>();
      info.status = 0;
      yield info;
    } else {
      var info = di.sl<Info>();
      info.status = 4;
      yield info;
      var response = await HttpGet.getHttp(event);
      if (response == "errorInput") {
        var info = di.sl<Info>();
        info.status = 1;
        yield info;
      } else if (response == "emptyField") {
        var info = di.sl<Info>();
        info.status = 2;
        yield info;
      } else {
        var info = toInfo(response);
        info.status = 5;
        yield info;
      }
    }
  }

  toInfo(Map map) {
    var info = di.sl<Info>();
    info = Info(map['main']['temp_max'], map['main']['temp_min'], map['name'],
        map['main']['pressure'], map['main']['humidity']);
    return info;
  }
}

class Info {
  var status = 0;
  var max;
  var min;
  var name;
  var pressure;
  var humidity;
  Info(
      [this.max,
      this.min,
      this.name,
      this.pressure,
      this.humidity,
      this.status]);
}
