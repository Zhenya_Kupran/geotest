import 'package:get_it/get_it.dart';
import 'package:my_test/city/logic.dart';

final sl = GetIt.instance;

init() {
  sl.registerFactory(() => Info());
  sl.registerFactory(() => MyBloc(Info()));
}
